from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # homepage
    url(r'^$', RedirectView.as_view(permanent=False, url='/application/user/'), name='home'),

    url(r'^application/', include('python_code_task.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
