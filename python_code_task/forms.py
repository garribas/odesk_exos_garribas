from django import forms

from .models import ApplicationUser


class ApplicationUserForm(forms.ModelForm):

    class Meta:
        model = ApplicationUser
        fields = ['username', 'email', 'birth_date']

    def clean_username(self):
        data = self.cleaned_data['username']
        print data

        #if self.user_id:
        #    duplicates = TestPrepUser.objects.exclude(pk=self.user_id).filter(email=data).count()
        #else:
        #    duplicates = TestPrepUser.objects.filter(email=data).count()

        #if duplicates > 0:
        #    raise forms.ValidationError(_('Username already exists, choose another email address.'))

        return data
