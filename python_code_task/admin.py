from django.contrib import admin

from .models import ApplicationUser


class ApplicationUserModelAdmin(admin.ModelAdmin):
    exclude = ('random_value', )


admin.site.register(ApplicationUser, ApplicationUserModelAdmin)
