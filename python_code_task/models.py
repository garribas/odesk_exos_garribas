from dateutil.relativedelta import relativedelta
from datetime import datetime
from random import randint

from django.contrib.auth.models import AbstractUser
from django.db import models


class ApplicationUser(AbstractUser):
    birth_date = models.DateField(blank=True, null=True)
    random_value = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        # calculate order if not specified
        if not self.id:
            self.random_value = randint(1, 100)
        super(ApplicationUser, self).save(*args, **kwargs)

    def is_allowed(self):
        return (relativedelta(datetime.now(), self.birth_date).years > 13)

    def bizzfuzz(self):
        if self.random_value % 3 == 0 and self.random_value % 5 == 0:
            return 'BizzFuzz'
        elif self.random_value % 3 == 0:
            return 'Bizz'
        elif self.random_value % 5 == 0:
            return 'Fuzz'
        else:
            return self.random_value

