import csv

from django.core.urlresolvers import reverse
from django.contrib import messages
from django.http import HttpResponse
from django.views.generic import View, FormView, DetailView, CreateView, UpdateView, ListView, DeleteView
from django.utils.translation import ugettext_lazy as _

from .forms import ApplicationUserForm
from .models import ApplicationUser


# a custom mixin to return a csv file based on a list of lists
# to avoid using non-class based views like in django's official doc
# code copied from here: https://godjango.com/blog/download-csv-files-via-csvresponemixin/
class CSVResponseMixin(object):
    csv_filename = 'csvfile.csv'

    def get_csv_filename(self):
        return self.csv_filename

    def render_to_csv(self, data):
        response = HttpResponse(content_type='text/csv')
        cd = 'attachment; filename="{0}"'.format(self.get_csv_filename())
        response['Content-Disposition'] = cd

        writer = csv.writer(response)
        for row in data:
            writer.writerow(row)

        return response


class ApplicationUserListView(ListView):
    model = ApplicationUser


class ApplicationUserCsvListView(CSVResponseMixin, View):
    csv_filename = 'users.csv'

    def get(self, request, *args, **kwargs):
        data = [(user.username, user.birth_date, user.is_allowed(), user.random_value, user.bizzfuzz())
                        for user in ApplicationUser.objects.all()]
        return self.render_to_csv(data)


class ApplicationUserDetailView(DetailView):
    model = ApplicationUser


class ApplicationUserCreateView(CreateView):
    model = ApplicationUser
    form_class = ApplicationUserForm

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, _(
            'A new user has been created.' \
        ))
        return reverse('application_user_list')


class ApplicationUserUpdateView(UpdateView):
    model = ApplicationUser
    form_class = ApplicationUserForm

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO,
                _('User with email %(email)s has been modified.') % {'email': self.object.email}
        )
        return reverse('application_user_list')


class ApplicationUserDeleteView(DeleteView):
    model = ApplicationUser

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, _(
            'User has been deleted.' \
        ))
        return reverse('application_user_list')
