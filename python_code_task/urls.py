from django.conf.urls import patterns, include, url

from .views import ApplicationUserListView, ApplicationUserCsvListView, \
                    ApplicationUserCreateView, ApplicationUserDetailView, \
                    ApplicationUserUpdateView, ApplicationUserDeleteView

urlpatterns = patterns('',
    url(r'user/$', ApplicationUserListView.as_view(), name='application_user_list'),
    url(r'user/csv$', ApplicationUserCsvListView.as_view(), name='application_user_csv_list'),
    url(r'user/create/$', ApplicationUserCreateView.as_view(), name='application_user_create'),
    url(r'user/(?P<pk>\d+)/$', ApplicationUserDetailView.as_view(), name='application_user_detail'),
    url(r'user/(?P<pk>\d+)/update$', ApplicationUserUpdateView.as_view(), name='application_user_update'),
    url(r'user/(?P<pk>\d+)/delete/$', ApplicationUserDeleteView.as_view(), name='application_user_delete'),
)
