from django import template

register = template.Library()

from python_code_task.models import ApplicationUser


@register.simple_tag
def allowed(value):
    if value:
        return 'allowed'
    else:
        return 'blocked'

@register.simple_tag
def bizzfuzz(value):
    user = ApplicationUser.objects.get(pk=value)
    return user.bizzfuzz()
