from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.test import TestCase

from .models import ApplicationUser


class ApplicationUserTestCase(TestCase):
    fixtures = ['users_data.json']

    def test_random_value_default_on_creation(self):
        user = ApplicationUser()
        user.username = 'test'
        user.save()

        self.assertNotEqual(ApplicationUser.objects.get(username='test').random_value, 0,
            'On creation no random value has been generated, default random value should had been overwritten'
        )

    def test_random_value_overwritten_on_creation(self):
        user = ApplicationUser()
        user.username = 'test'
        user.random_value = 50
        user.save()

        self.assertNotEqual(ApplicationUser.objects.get(username='test').random_value, 50,
            'On creation no random value has been generated, existing random value should had been overwritten'
        )

    def test_random_value_limits_on_creation(self):
        for i in xrange(1000):
            username = 'test' + str(i)
            user = ApplicationUser()
            user.username = username
            user.save()

            self.assertTrue((1 <= ApplicationUser.objects.get(username=username).random_value <= 100),
                'Random value must between 1 and 100, was ' + str(ApplicationUser.objects.get(username=username).random_value)
            )

    def test_random_value_on_update(self):
        user = ApplicationUser.objects.get(pk=1)
        user.random_value = 1
        user.save()

        self.assertEqual(ApplicationUser.objects.get(pk=1).random_value, 1,
            'On update no random value should be generated, random value was overwritten'
        )

    def test_is_allowed(self):
        user = ApplicationUser()
        user.birth_date = datetime.now() - relativedelta(years=1)
        self.assertFalse(user.is_allowed())

        user.birth_date = datetime.now() - relativedelta(years=13)
        self.assertFalse(user.is_allowed())

        user.birth_date = datetime.now() - relativedelta(years=14)
        self.assertTrue(user.is_allowed())

    def test_bizzfuzz(self):
        user = ApplicationUser()
        user.random_value = 3
        self.assertEqual(user.bizzfuzz(), 'Bizz')

        user.random_value = 9
        self.assertEqual(user.bizzfuzz(), 'Bizz')

        user.random_value = 5
        self.assertEqual(user.bizzfuzz(), 'Fuzz')

        user.random_value = 20
        self.assertEqual(user.bizzfuzz(), 'Fuzz')

        user.random_value = 15
        self.assertEqual(user.bizzfuzz(), 'BizzFuzz')

        user.random_value = 16
        self.assertEqual(user.bizzfuzz(), user.random_value)
