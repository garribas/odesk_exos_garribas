Installation instructions:

virtualenv --no-site-packages .env
source .env/bin/activate
pip install -r requirements.txt
python manage.py syncdb
python manage.py migrate
python manage.py test
python manage.py runserver
